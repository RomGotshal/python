import copy

def print_word(game_word):
    output = ""
    for index in range(len(game_word)):
        output += game_word[index] + " "
    print(output)

def create_game_word(goal, game_word):
    for index in range(len(goal)):
        game_word[index] = "_"

def replace_chars(goal, game_word, char_from_user):
    for index in range(len(game_word)):
        if goal[index] == char_from_user:
            game_word[index] = char_from_user

def check_for_win(goal, game_word):
    if goal == ''.join(game_word):
        return True
    return False


def turn(goal, already_tried, game_word):
    char_from_user = input("please guess a letter ").upper()
    if(already_tried.count(char_from_user)):
        print("already tried")
    elif(goal.count(char_from_user)):
        print("exist")
        already_tried.append(char_from_user)
        replace_chars(goal, game_word, char_from_user)
    else:
        print("bad try")
        already_tried.append(char_from_user)


#main
print("Welcome to Hangman!")
goal = "EVAPORATE"
game_word = list(copy.deepcopy(goal))
already_tried = []



create_game_word(goal, game_word)
while not check_for_win(goal, game_word):
    print_word(game_word)
    turn(goal, already_tried, game_word)
else:
    print("yes!!!! the word is: {}".format(goal))