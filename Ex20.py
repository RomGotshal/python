def is_inside(list, number):
    return number in list

#main
list1 = [1,2,3,5,6,8,9]
list2 = [1,2,3,5,6,7,8,9]
print(is_inside(list1,7))
print(is_inside(list2,7))
