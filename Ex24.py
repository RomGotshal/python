#the board design (take 5 spaces)
ceiling = " ---"
wall = "|   "
def print_board(size):
    board = ""
    for row in range(size):
        for col in range(size):     #the rows of the ---
            board += ceiling
        board += "\n"
        for col in range(size):     #the rows of the |
            board += wall
        board += "|\n"              #the right |
    for col in range(size):         #the row of the flore
        board += ceiling
    print(board)
    
#main
print_board(3)