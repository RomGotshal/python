import requests 
from bs4 import BeautifulSoup
import json
import datetime

file = open("head_lines_nytimes.txt", "w")

url_nytimes = "https://www.nytimes.com/"
request = requests.get(url_nytimes)
date_now = datetime.datetime.now()
date_now_format = date_now.strftime('%A, %B %d, %Y  %I:%M %p')

request_html = request.text
soup = BeautifulSoup(request_html, "html.parser")

scripts = soup.find_all('script')
for script in scripts:
    if 'preloadedData' in script.text:
        json_str = script.text
        json_str = json_str.split('=', 1)[1].strip()
        json_str = json_str.rsplit(';', 1)[0]
        json_element = json.loads(json_str)


file.write ('{0}\nHeadlines\n{1}\n'.format(url_nytimes, date_now_format))
count = 1
for ele, v in json_element['initialState'].items():
    try:
        if v['headline'] and v['__typename'] == 'PromotionalProperties':
            file.write('Headline {0}: {1}\n'.format(count, v['headline']))
            count += 1
    except:
        continue