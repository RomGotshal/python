import copy


def check_small_array(arr):  # check if all the cells in the arr are the same
    if arr.count(1) == len(arr):
        return 1
    if arr.count(2) == len(arr):
        return 2


def check_rows(game):
    for row in range(len(game)):
        check_for_winner = check_small_array(
            game[row])  # send every row to be checked
        if check_for_winner:
            return check_for_winner


def check_cols(game):
    new_game = copy.deepcopy(game)
    for row in range(len(game)):  # change col to be in a row
        for col in range(len(game)):
            new_game[col][row] = game[row][col]
    winner = check_rows(new_game)  # using exist function that checks row
    if winner:
        return winner


def check_diags(game):
    diag_rtl = []
    diag_ltr = []
    for row in range(len(game)):
        for col in range(len(game)):
            if row == col:  # create list from diag ltr
                diag_ltr.append(game[row][col])
            if row + col == len(game) - 1:  # create list from diag rtl
                diag_rtl.append(game[row][col])

    check_for_winner = check_small_array(
        diag_ltr)  # sent the diag to be cheacked
    if check_for_winner:
        return check_for_winner
    check_for_winner = check_small_array(
        diag_rtl)  # sent the diag to be cheacked
    if check_for_winner:
        return check_for_winner


def check_for_win(game):  # 1 is player-1, 2 is player-2, none is no victory yet
    winner = check_rows(game)  # check the win by rows
    if winner:
        return winner
    winner = check_cols(game)  # check the win by cols
    if winner:
        return winner
    winner = check_diags(game)  # check the win by diags
    if winner:
        return winner


# main
game_none = [[0, 2, 0],
             [2, 1, 0],
             [2, 1, 1]]

winner_is_2_col = [[2, 2, 0],
                   [2, 1, 0],
                   [2, 1, 1]]

winner_is_2_diag_ltr = [[2, 1, 0],
                        [1, 2, 0],
                        [1, 2, 2]]

winner_is_1_diag_rtl = [[0, 2, 1],
                        [2, 1, 0],
                        [1, 1, 2]]

winner_is_1_row = [[1, 1, 1],
                   [2, 1, 0],
                   [2, 1, 1]]


print(check_for_win(winner_is_1_row))
print(check_for_win(winner_is_2_col))
print(check_for_win(winner_is_2_diag_ltr))
print(check_for_win(winner_is_1_diag_rtl))
print(check_for_win(game_none))
