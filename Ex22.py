def count_names(file):
    list_of_names = []
    list_of_counters = []
    for name in file:
        name = name.capitalize()
        if name[len(name)-1] == "\n":   # if the name end in back slash n
            name = name[:len(name)-1] 
        if name not in list_of_names:   # if the name isnt existed in the list
            list_of_names.append(name)
            list_of_counters.append(1)
        else:
            list_of_counters[list_of_names.index(name)] += 1
    for name in list_of_names:
        print("the name: {0}, is in {1} times"
        .format(name, list_of_counters[list_of_names.index(name)]))


#main
file = open("a.txt", "r")
count_names(file)
file.close()