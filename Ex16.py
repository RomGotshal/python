import random

def generatePassword():
    password = ""
    size = random.randrange(5, 15)
    for i in range(size):
        rnd = random.randrange(33, 126)
        while rnd == 92: # 92 is back slash
            rnd = random.randrange(33, 126)
        password += chr(rnd)
    return password

#main
print(generatePassword())