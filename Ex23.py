def file_to_list(file):
    list_answer = []
    for item in file:
        if item[len(item)-1] == "\n":   # if the number end in back slash n
                item = item[:len(item)-1] 
        list_answer.append(item)
    return list_answer

def overlapping_numbers(file1, file2):
    numbers_from_file1 = file_to_list(file1)
    numbers_from_file2 = file_to_list(file2)
    overlapping_list = []

    for number in numbers_from_file1:
        if number in numbers_from_file2:
            overlapping_list.append(number)
    return overlapping_list
    

    


#main
file_prime = open("prime.txt", "r")
file_happy_numbers = open("happy_numbers.txt", "r")
print(overlapping_numbers(file_prime, file_happy_numbers))
file_prime.close()
file_happy_numbers.close()