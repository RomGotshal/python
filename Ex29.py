import copy


# the board design (take 5 spaces) - the board has space inside
ceiling = " ---"
wall_before = "| "
wall_after = " "


def print_board(game):
    board = ""
    for row in range(len(game)):
        for col in range(len(game)):  # the rows of the ---
            board += ceiling
        board += "\n"
        for col in range(len(game)):  # the rows of the |
            board += wall_before + str(game[row][col]) + wall_after
        board += "|\n"  # the right |
    for col in range(len(game)):  # the row of the flore
        board += ceiling
    print(board)


def check_small_array(arr):  # check if all the cells in the arr are the same
    if arr.count(1) == len(arr):
        return 1
    if arr.count(2) == len(arr):
        return 2


def check_rows(game):
    for row in range(len(game)):
        check_for_winner = check_small_array(
            game[row])  # send every row to be checked
        if check_for_winner:
            return check_for_winner


def check_cols(game):
    new_game = copy.deepcopy(game)
    for row in range(len(game)):  # change col to be in a row
        for col in range(len(game)):
            new_game[col][row] = game[row][col]
    winner = check_rows(new_game)  # using exist function that checks row
    if winner:
        return winner


def check_diags(game):
    diag_rtl = []
    diag_ltr = []
    for row in range(len(game)):
        for col in range(len(game)):
            if row == col:  # create list from diag ltr
                diag_ltr.append(game[row][col])
            if row + col == len(game) - 1:  # create list from diag rtl
                diag_rtl.append(game[row][col])

    check_for_winner = check_small_array(
        diag_ltr)  # sent the diag to be cheacked
    if check_for_winner:
        return check_for_winner
    check_for_winner = check_small_array(
        diag_rtl)  # sent the diag to be cheacked
    if check_for_winner:
        return check_for_winner


def check_for_win(game):  # 1 is player-1, 2 is player-2, none is no victory yet
    winner = check_rows(game)  # check the win by rows
    if winner:
        return winner
    winner = check_cols(game)  # check the win by cols
    if winner:
        return winner
    winner = check_diags(game)  # check the win by diags
    if winner:
        return winner


def play_turn(game, turn_counter):
    can_be_placed = False
    player = turn_counter % 2 + 1
    while not can_be_placed:
        print("turn of player {}".format(player))
        cordinats = input("please enter row,col ")
        cordinats = cordinats.split(",")
        x = int(cordinats[0]) - 1
        y = int(cordinats[1]) - 1
        if game[x][y] == 0:
            game[x][y] = player
            can_be_placed = True
        else:
            print("please pick an empty spot ")


            
# main
game_boared = [[0, 0, 0],
               [0, 0, 0],
               [0, 0, 0]]

turn_counter = 0


for turn in range(len(game_boared) * len(game_boared)):
    winner = check_for_win(game_boared)
    if winner:
        print("the winner is player {} ".format(winner))
        break
    else:
        print_board(game_boared)
        play_turn(game_boared, turn)
else:
    print("tie")        
