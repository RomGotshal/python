#!/usr/bin/env python
# coding: utf-8

# In[13]:


a = [1, 1, 1, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 56, 56]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
for num in a:
    while a.count(num) > 1:
        a.remove(num)
c = [num for num in a if num in b]
print(c)


# In[ ]:




