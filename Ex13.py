def fibo(numOfFibo):
    if numOfFibo == 0:
        fibList = []
    elif numOfFibo == 1:
        fibList = [1]
    elif numOfFibo == 2:
        fibList = [1,1]
    elif numOfFibo > 2:
        fibList = [1,1]
        i = 1
        while i < (numOfFibo - 1):
            fibList.append(fibList[i] + fibList[i-1])
            i += 1

    return fibList


# main
numberOfFiboNumbers = int(input("please enter the number of fibo nubers you want "))
print(fibo(numberOfFiboNumbers))
