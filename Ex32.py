import copy
import random

def rnd_word(file):
    words = file.readlines()                    #take the file to an list
    word = words[random.randrange(len(words))]  #takes random word
    return word[0: len(word) -1]                #without the back slash n



def print_word(game_word):
    output = ""
    for index in range(len(game_word)):
        output += game_word[index] + " "
    print(output)

def create_game_word(goal, game_word):
    for index in range(len(goal)):
        game_word[index] = "_"

def replace_chars(goal, game_word, char_from_user):
    for index in range(len(game_word)):
        if goal[index] == char_from_user:
            game_word[index] = char_from_user

def check_for_win(goal, game_word, mistakes):
    if goal == ''.join(game_word) or len(mistakes) == 0:
        return True
    return False


def turn(goal, already_tried, game_word, mistakes):
    char_from_user = input("please guess a letter ").upper()
    if(already_tried.count(char_from_user)):
        print("already tried")
    elif(goal.count(char_from_user)):
        print("exist")
        already_tried.append(char_from_user)
        replace_chars(goal, game_word, char_from_user)
    else:
        print("bad try, you now on the {} stage ".format(mistakes[-1]))
        mistakes.pop(len(mistakes) - 1)
        already_tried.append(char_from_user)


#main
file_sowpods = open("sowpods.txt", "r")
goal = rnd_word(file_sowpods)
file_sowpods.close()

print("for me :)  the word is: {}".format(goal))

print("Welcome to Hangman!")
game_word = list(copy.deepcopy(goal))
already_tried = []
mistakes  = ["second arm", "first arm", "second leg", "first leg", "body", "head"]
print("you have only {} guesses ".format(len(mistakes)))

create_game_word(goal, game_word)
while not check_for_win(goal, game_word, mistakes):
    print_word(game_word)
    turn(goal, already_tried, game_word, mistakes)
else:
    if len(mistakes):
        print("yes!!!! the word is: {}".format(goal))
    else:
        print("im sorry, but you lost. the word is: {}".format(goal))