#!/usr/bin/env python
# coding: utf-8

# In[5]:


def getListOfDivisors(numberFromUser):
    answer = []
    for number in range(1, numberFromUser + 1):
        if (numberFromUser % number) == 0:
            answer.append(number)
    return answer

#main
numberFromUser = int(input("please enter a number"))
if len(getListOfDivisors(numberFromUser)) == 2:
    print("the number is prime")
else:
    print("the number is not prime")


# In[ ]:





# In[ ]:




