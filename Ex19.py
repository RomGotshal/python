import requests
from bs4 import BeautifulSoup

base_url = "https://www.vanityfair.com/style/society/2014/06/monica-lewinsky-humiliation-culture"
request_url = requests.get(base_url)
soup = BeautifulSoup(request_url.text, "html.parser")

all_p_in_article = soup.select("p")

for element in all_p_in_article[7:]:
  print(element.text)