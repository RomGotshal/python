import random

def rnd_word(file):
    words = file.readlines()                    #take the file to an list
    word = words[random.randrange(len(words))]  #takes random word
    return word[0: len(word) -1]                #without the back slash n

#main
file_sowpods = open("sowpods.txt", "r")
print(rnd_word(file_sowpods))
file_sowpods.close()