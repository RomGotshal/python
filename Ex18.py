import random

def cows(goal, guess):
    goal = str(goal)
    guess = str(guess)
    count = 0
    for index in range(0, len(goal)):
        if goal[index] == guess[index]:
            count += 1
    return count    


def bulls(goal, guess):
    goal = str(goal)
    guess = str(guess)
    count = 0
    for dig in goal:
        if dig in guess:
            count += 1
    return count


def game():
    goal = random.randrange(1000, 10000)
    print("shhhhhhh... but the goal is: " + str(goal))
    guess = int(input("enter a guess: "))
    while guess != goal:
        cowsCount = cows(goal, guess)
        bullsCount = bulls(goal, guess) - cowsCount
        print("you have " + str(cowsCount) + " cows, and " + str(bullsCount) + " bulls")
        guess = int(input("enter a guess: "))
    print("yes!! the number is: " + str(goal))
#main
game()
