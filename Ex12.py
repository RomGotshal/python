def listOfFirstAndLast(list):
    newList = []
    newList.append(list[0])
    newList.append(list[-1])
    return newList

#main
a = [5, 10, 15, 20, 25]
print(listOfFirstAndLast(a))